package com.crisxux.rsa.controllers;

import java.math.BigInteger;
import java.util.StringTokenizer;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crisxux.rsa.models.dto.NFiNPublicaPrivadaDto;
import com.crisxux.rsa.models.dto.ParPrimosDto;
import com.crisxux.rsa.models.dto.RequestCifrarDto;
import com.crisxux.rsa.models.dto.RequestDescifrarDto;
import com.crisxux.rsa.models.dto.ResponseCifradoDescifradoDto;
import com.crisxux.rsa.util.UtilRSA;

//@CrossOrigin(origins = "http://localhost:8080")
@RestController
@RequestMapping(value = "/api")
public class PrincipalController {

	@GetMapping(value = "/generar_primos")
	public ParPrimosDto generarPrimos() {
		ParPrimosDto parPrimosDto = new ParPrimosDto();
		UtilRSA utilRSA = new UtilRSA(10);

		parPrimosDto.setPrimoP(utilRSA.getP());
		parPrimosDto.setPrimoQ(utilRSA.getQ());
		return parPrimosDto;
	}

	@PostMapping(value = "/generar_n_fin_publica_privada")
	public NFiNPublicaPrivadaDto generarNFiNPublicaPrivada(@RequestBody ParPrimosDto parPrimosDto) {
		NFiNPublicaPrivadaDto nFiNPublicaPrivadaDto = new NFiNPublicaPrivadaDto();
		UtilRSA utilRSA = new UtilRSA(parPrimosDto.getPrimoP(), parPrimosDto.getPrimoQ(), 10);

		nFiNPublicaPrivadaDto.setN(utilRSA.getN());
		nFiNPublicaPrivadaDto.setFiN(utilRSA.getTotient());
		nFiNPublicaPrivadaDto.setPublica(utilRSA.getE());
		nFiNPublicaPrivadaDto.setPrivada(utilRSA.getD());

		return nFiNPublicaPrivadaDto;
	}

	@PostMapping(value = "/cifrar_mensaje")
	public ResponseCifradoDescifradoDto cifrarMensaje(@RequestBody RequestCifrarDto requestCifrarDto) {
		ResponseCifradoDescifradoDto responseCifradoDescifradoDto = new ResponseCifradoDescifradoDto();
		String concatenacion = "";
		UtilRSA utilRSA = new UtilRSA();

		BigInteger[] textoCifrado = utilRSA.encripta(requestCifrarDto.getMensaje(), requestCifrarDto.getPublica(),
				requestCifrarDto.getN());

		for (int i = 0; i < textoCifrado.length; i++) {
			concatenacion = concatenacion + textoCifrado[i].toString() + "\t";
			if (i != textoCifrado.length - 1) {
				concatenacion = concatenacion + "";
			}
		}

		responseCifradoDescifradoDto.setMensaje(concatenacion);
		return responseCifradoDescifradoDto;
	}

	@PostMapping(value = "/descifrar_mensaje")
	public ResponseCifradoDescifradoDto descifrarMensaje(@RequestBody RequestDescifrarDto requestDescifrarDto) {
		ResponseCifradoDescifradoDto responseCifradoDescifradoDto = new ResponseCifradoDescifradoDto();
		UtilRSA utilRSA = new UtilRSA();

		String letra = "";

		StringTokenizer st = new StringTokenizer(requestDescifrarDto.getMensaje());
		BigInteger[] textoCifrado = new BigInteger[st.countTokens()];

		for (int i = 0; i < textoCifrado.length; i++) {
			letra = st.nextToken();
			textoCifrado[i] = new BigInteger(letra);
		}

		responseCifradoDescifradoDto.setMensaje(utilRSA.desencripta(textoCifrado,  requestDescifrarDto.getPrivada(), requestDescifrarDto.getN()));

		return responseCifradoDescifradoDto;
	}
}