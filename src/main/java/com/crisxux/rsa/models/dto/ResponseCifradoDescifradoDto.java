package com.crisxux.rsa.models.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ResponseCifradoDescifradoDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String mensaje;

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
