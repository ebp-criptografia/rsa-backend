package com.crisxux.rsa.models.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class RequestDescifrarDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private BigInteger privada;
	private BigInteger n;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public BigInteger getPrivada() {
		return privada;
	}
	public void setPrivada(BigInteger privada) {
		this.privada = privada;
	}
	public BigInteger getN() {
		return n;
	}
	public void setN(BigInteger n) {
		this.n = n;
	}
	
}
