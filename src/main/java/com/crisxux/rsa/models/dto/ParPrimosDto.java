package com.crisxux.rsa.models.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class ParPrimosDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigInteger primoP;
	private BigInteger primoQ;
	
	public BigInteger getPrimoP() {
		return primoP;
	}
	public void setPrimoP(BigInteger primoP) {
		this.primoP = primoP;
	}
	public BigInteger getPrimoQ() {
		return primoQ;
	}
	public void setPrimoQ(BigInteger primoQ) {
		this.primoQ = primoQ;
	}
	
}
