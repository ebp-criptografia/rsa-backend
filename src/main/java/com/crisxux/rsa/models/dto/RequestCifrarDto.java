package com.crisxux.rsa.models.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class RequestCifrarDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String mensaje;
	private BigInteger publica;
	private BigInteger n;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public BigInteger getPublica() {
		return publica;
	}
	public void setPublica(BigInteger publica) {
		this.publica = publica;
	}
	public BigInteger getN() {
		return n;
	}
	public void setN(BigInteger n) {
		this.n = n;
	}
	
	
	
	
}
