package com.crisxux.rsa.models.dto;

import java.io.Serializable;
import java.math.BigInteger;

public class NFiNPublicaPrivadaDto implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private BigInteger n;
	private BigInteger fiN;
	private BigInteger publica;
	private BigInteger privada;
	public BigInteger getN() {
		return n;
	}
	public void setN(BigInteger n) {
		this.n = n;
	}
	public BigInteger getFiN() {
		return fiN;
	}
	public void setFiN(BigInteger fiN) {
		this.fiN = fiN;
	}
	public BigInteger getPublica() {
		return publica;
	}
	public void setPublica(BigInteger publica) {
		this.publica = publica;
	}
	public BigInteger getPrivada() {
		return privada;
	}
	public void setPrivada(BigInteger privada) {
		this.privada = privada;
	}
	
}
